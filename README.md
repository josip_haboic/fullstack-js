# Fullstack Node Boilerplate

## Front end

* Vue
* Vue Router
* Vuex
* AnimeJs
* GraphQL
* Vue Apollo

---

## Back end

* Koa
* TypeORM
* GraphQL