import { ApolloServer } from 'apollo-server-koa'

import { schema } from './schema'


export const apolloServer = new ApolloServer({
  schema
})
