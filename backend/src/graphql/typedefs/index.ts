
const schema = `
  schema {
    query: Query
    mutation: Mutation
  }

  type Query {
    _query: Boolean
  }

  type Mutation {
    _mutation: Boolean
  }
`

export const typeDefs = [schema]
