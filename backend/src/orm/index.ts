import { createConnection } from 'typeorm'


export const intializeDatabase = async () => {
  return createConnection({
    type: 'sqlite',
    database: './src/storage/database.db',
    // entities: [`${__dirname}/models/*.model.ts`],
    // subscribers: [`${__dirname}/subscribers/*.subscriber.ts`],
    logging: [
      // 'query',
      'error'
    ],
    synchronize: true,
  }).then((connection: any) => {
    debugger

    return connection
  })
}
