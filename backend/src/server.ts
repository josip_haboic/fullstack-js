import * as Koa from 'koa';
import { apolloServer } from './graphql/server'
import { intializeDatabase } from './orm'


/**
 * Initialize and start application
 */
const bootstrap = async () => {

  // connect to database
  await intializeDatabase()

  // create koa application
  const app = new Koa()

  // set database connection to app.context so we can use database connection
  // everywhere in application
  // Object.defineProperty(app.context, 'db', {
  //   get: () => dbConnection
  // })

  // connect apollo server to the koa application
  apolloServer.applyMiddleware({ app })

  // start application
  app.listen(3000)


}

// initialize and start application
bootstrap()
