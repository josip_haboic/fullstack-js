import { ApolloClient } from "apollo-client";
import VueApollo from "vue-apollo";
import { HttpLink } from "apollo-link-http";
import Vue from "vue";
import { InMemoryCache } from "apollo-cache-inmemory";

const httpLink = new HttpLink({
  uri: "http://localhost:3000/graphql",
});

export const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
  connectToDevTools: true
});

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});

Vue.use(VueApollo);
