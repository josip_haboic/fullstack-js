const crypto = require("crypto");

export default {
    uuid16: () => {
        return [4, 2, 2, 2, 6].map((size) => crypto.randomBytes(size).toString('hex')).join('-');
    },
    uuid8: () => {
        return [2, 1, 1, 1, 3].map((size) => crypto.randomBytes(size).toString('hex')).join('-');
    }
}

