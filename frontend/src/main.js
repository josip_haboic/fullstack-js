import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import { store } from './store'
import './registerServiceWorker'
import { apolloProvider } from './apollo'
import Notifications from './plugins/notify'
import AnimeJS from './plugins/anime'


Vue.config.productionTip = false;


Vue.use(Notifications)
Vue.use(AnimeJS)

new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount('#app');
