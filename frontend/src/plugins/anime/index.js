import anime from 'animejs'


export default {
    install(Vue, options) {
        Vue.directive('anime', {
            bind: function bind(targets, binding) {
                const opts = Object.assign({}, binding.value, {targets})
                anime(opts)
            }
        })
        Vue.prototype.$anime = anime
    }
}