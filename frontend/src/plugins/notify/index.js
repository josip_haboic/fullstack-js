export default {
    install(Vue, options) {
        let notifications = []
        Vue.prototype.$notifications = notifications
        Vue.prototype.$notify = {
            notify: (notification) => notifications.push(notification),
            clear: (notification) => {
                const index = notifications.indexOf(notification)
                if (index !== -1) {
                    notifications.splice(index, 1)
                }
            },
            clearGroup: (group) => {
                notifications.map((n) => {
                    if (n.group === group) {
                        const index = notifications.indexOf(n)
                        if (index !== -1) {
                            notifications.splice(index)

                        }
                    }
                })
            },
            clearAll: () => notifications = []
        }
    }
}